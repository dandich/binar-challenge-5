const express = require('express');
const app = express()
const port = 3000
const path = require("path");
const users = require('./users').users
const bodyParser = require("body-parser");
const { application } = require('express');

const jsonParser = bodyParser.json();

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(express.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, 'public')))

// middleware
const logger = (req, res, next) => {
    console.log(`Requesting: ${req.method} - ${req.url}`);
    next();
}

app.get("", logger, (req, res) => {
    res.sendFile(path.join(__dirname, '/', './views', '/landing.html'));
});

app.get("/game.html", logger, (req, res) => {
    res.sendFile(path.join(__dirname, '/', './views', '/game.html'));
});

app.get('/users', logger, jsonParser, (req, res) => {
    res.json(users)
})

app.get("/signup.html", logger, (req, res) => {
    res.sendFile(path.join(__dirname, '/', './views', '/signup.html'))
})
// signup post
app.post("/register", logger, (req, res) => {
    let newUser = {
        id: Date.now(),
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
    };
    users.push(newUser);
    console.log('User list', users);

    res.sendFile(path.join(__dirname, '/', '/views', '/login.html'))

})

app.get("/login.html", logger, (req, res) => {
    res.sendFile(path.join(__dirname, '/', '/views', '/login.html'))
})
// login post
app.post("/login", logger, (req, res) => {
    let foundUser = users.find((data) => req.body.email === data.email);
    if (foundUser) {
        let passwordMatch = users.find((data) => req.body.password === data.password)
        if (passwordMatch) {
            let useremail = foundUser.email
            let usrname = foundUser.username;
            res.send(`<div align ='center'><h2>login successful</h2></div><br><br><br><div align ='center'><h3>You are Logged in<br><br>User Details:<br>Username: ${usrname}<br>Email: ${useremail}</h3></div><br><br><div align='center'><a href='./login.html'>logout</a></div> <div align='center'><a href='./users'>JSON users data`);
        } else {
            res.send("<div align ='center'><h2>Invalid email or password</h2></div><br><br><div align ='center'><a href='./login.html'>login again</a></div>");
        }
    } else {
        res.send("<div align ='center'><h2>Invalid email or password</h2></div><br><br><div align ='center'><a href='./login.html'>login again</a></div>")
    }
})

app.listen(port, () => {
    console.log(`This app is listening on port ${port}`)
})
